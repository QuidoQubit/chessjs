

function attackableSqsPerFigType(fig,figBoard,sqBoard) {
// vrati vsechna kontrolovana pole dane figurky jako sqIds
		let type;
		let color;
		if (typeof fig === 'string'){type = figIdtoType(fig); color = fig.charAt(fig.length-2)}else{type = fig.type; color = fig.color;};
		let sqIds;
		if (type == 'Rook'){sqIds = [].concat(rawSqueresAround(fig,figBoard,sqBoard),colSqueresAround(fig,figBoard,sqBoard));};
		if (type == 'King'){sqIds = [].concat(diagOneSqueresAround(fig,figBoard),colRawOneSqueresAround(fig,figBoard));};
		if (type == 'Queen'){sqIds = [].concat([].concat(rawSqueresAround(fig,figBoard,sqBoard),colSqueresAround(fig,figBoard,sqBoard),diagSqueresAround(fig,figBoard,sqBoard)));};
		if (type == 'Pawn'){sqIds = pawnSqueresAround(fig,figBoard,sqBoard);};
		if (type == 'Bishop'){sqIds = diagSqueresAround(fig,figBoard,sqBoard);};
		if (type == 'Horse'){sqIds = horseSqueresAround(fig,figBoard);};
		//sqIds = removeSameColorFigSqs(color,sqIds);
		return sqIds
};


function checkIfPieceOnSq(sqIds,sqBoard) {
	let res = [];
	for (i = 0; i < sqIds.length; i++ ){
		let s = sqIds[i];
		if (sqBoard[s] != 0){res.push(sqIds[i])};
	};
	return res
};

function validSqIds(sqIds) { //jen kontroluje ze delka ids je 2
	let res = [];
	for (i=0;i<sqIds.length;i++){if (sqIds[i].length == 2) {res.push(sqIds[i]);}; };
	return res
}

function diagSqueresAround(fig,figBoard,sqBoard) { //toto snad lze skratit...
	let position = pos(fig,figBoard);
	let col = position[0];
	let num = position[1];
	var diagRightUp = [];
	let colInd = colList.indexOf(col);
	let numInd = numList.indexOf(num);

	diagRightUpCols = colList.slice(colInd+1,colList.length);
	diagRightUpNums = numList.slice(numInd+1,numList.length);
	diagLenUp = Math.min(diagRightUpCols.length,diagRightUpNums.length);
	diagRightUp = [];
	for (i = 0; i < diagLenUp; i++){
		diagRightUp.push(diagRightUpCols[i]+diagRightUpNums[i]);
	}
	diagRightDownCols = colList.slice(0,colInd);
	diagRightDownNums = numList.slice(0,numInd);
	diagLenDown = Math.min(diagRightDownCols.length,diagRightDownNums.length);
	diagRightDownCols = diagRightDownCols.reverse();
	diagRightDownNums = diagRightDownNums.reverse();
	diagRightDown = [];
	for (i = 0; i < diagLenDown; i++){
		diagRightDown.push(diagRightDownCols[i]+diagRightDownNums[i]);
	}
	diagRightDown = diagRightDown.reverse();
	let diagRight = [].concat(diagRightDown,diagRightUp);
	/////////////////////////////////////////////////////////
	diagLeftUpCols = colList.slice(0,colInd);
	diagLeftUpNums = numList.slice(numInd+1,numList.length);
	diagLenUp = Math.min(diagLeftUpCols.length,diagLeftUpNums.length);
	diagLeftUp = [];
	diagLeftUpCols = diagLeftUpCols.reverse();
	for (i = 0; i < diagLenUp; i++){
		diagLeftUp.push(diagLeftUpCols[i]+diagLeftUpNums[i]);
	}
	diagLeftDownCols = colList.slice(colInd+1,colList.length);
	diagLeftDownNums = numList.slice(0,numInd);
	diagLenDown = Math.min(diagLeftDownCols.length,diagLeftDownNums.length);
	diagLeftDownNums = diagLeftDownNums.reverse();
	diagLeftDown = [];
	for (i = 0; i < diagLenDown; i++){
		diagLeftDown.push(diagLeftDownCols[i]+diagLeftDownNums[i]);
	}
	diagLeftDown = diagLeftDown.reverse();
	let diagLeft = [].concat(diagLeftDown,diagLeftUp);
	let allDiagSqs = [].concat(diagRight,diagLeft);
    /*
    zde je to tak dlouhe, protoze koukam na omezeni figurkama
    */
	let figsOnLeftDown = checkIfPieceOnSq(diagLeftDown,sqBoard);
	let figsOnLeftUp = checkIfPieceOnSq(diagLeftUp,sqBoard);
	let downLeftInd = diagLeft.indexOf(figsOnLeftDown.at(-1));
	let upLeftInd = diagLeft.indexOf(figsOnLeftUp[0]);
	if (downLeftInd == -1){downLeftInd =0;};
	if (upLeftInd == -1){upLeftInd = diagLeft.length;};
	let leftFigDiag = diagLeft.slice(downLeftInd,upLeftInd+1);

	let figsOnRightDown = checkIfPieceOnSq(diagRightDown,sqBoard);
	let figsOnRightUp = checkIfPieceOnSq(diagRightUp,sqBoard);
	let downRightInd = diagRight.indexOf(figsOnRightDown.at(-1));
	let upRightInd = diagRight.indexOf(figsOnRightUp[0]);
	if (downRightInd == -1){downRightInd =0;};
	if (upRightInd == -1){upRightInd = diagRight.length;};
	let rightFigDiag = diagRight.slice(downRightInd,upRightInd+1);

	return [].concat(leftFigDiag,rightFigDiag)
};

function colSqueresAround(fig,figBoard,sqBoard) {
	let position = pos(fig,figBoard);
	let col = position[0];
	let num = position[1];
	let colInd = colList.indexOf(col);
	let numInd = numList.indexOf(num);
	let all = [col+'1',col+'2',col+'3',col+'4',col+'5',col+'6',col+'7',col+'8'];
	let downSqs = all.slice(0,numInd);
	let upSqs = all.slice(numInd+1,all.length);
	let figsOnDown = checkIfPieceOnSq(downSqs,sqBoard);
	let figsOnUp = checkIfPieceOnSq(upSqs,sqBoard);

	let downInd = all.indexOf(figsOnDown.at(-1));
	let upInd = all.indexOf(figsOnUp[0]);
	if (downInd == -1){downInd =0;};
	if (upInd == -1){upInd = all.length;};
	let fullRes = all.slice(downInd,upInd+1);
	return fullRes
};

function rawSqueresAround(fig,figBoard,sqBoard) {
	let position = pos(fig,figBoard);
	let num = position[1];
	let col = position[0];
	let colInd = colList.indexOf(col);
	let numInd = numList.indexOf(num);
	let all = ['A'+num,'B'+num,'C'+num,'D'+num,'E'+num,'F'+num,'G'+num,'H'+num];

	let leftSqs = all.slice(0,colInd);
	let rightSqs = all.slice(colInd+1,all.length);
	let figsOnLeft = checkIfPieceOnSq(leftSqs,sqBoard);
	let figsOnRight = checkIfPieceOnSq(rightSqs,sqBoard);

	let leftInd = all.indexOf(figsOnLeft.at(-1));
	let rightInd = all.indexOf(figsOnRight[0]);
	if (leftInd == -1){leftInd =0;};
	if (rightInd == -1){rightInd = all.length;};
	let fullRes = all.slice(leftInd,rightInd+1);
	return fullRes
};

function colRawOneSqueresAround(fig,figBoard) {
	let position = pos(fig,figBoard);
	let col = position[0];
	let num = position[1];
	let colInd = colList.indexOf(col);
	let numInd = numList.indexOf(num);
	let res =
	[colList[colInd]+numList[numInd-1],
	colList[colInd]+numList[numInd+1],
	colList[colInd-1]+numList[numInd],
	colList[colInd+1]+numList[numInd],];
	res = validSqIds(res);
	return res
};

function pawnSqueresAround(fig,figBoard,sqBoard) {
	let position = pos(fig,figBoard);
	let col = position[0];
	let num = position[1];
	let colInd = colList.indexOf(col);
	let numInd = numList.indexOf(num);
	var res = [];
	let killerPos = [];
	if ((fig.charAt(fig.length-2) == 'w')){
		if (num == 2 && checkIfPieceOnSq([col+numList[numInd+1],col+numList[numInd+2]],sqBoard).length == 0)
		{res = [col+numList[numInd+1],col+numList[numInd+2]]};

		if (checkIfPieceOnSq([col+numList[numInd+1]],sqBoard).length == 0)
		{res.push(col+numList[numInd+1]);};

		let killerPos0 = [colList[colInd-1]+numList[numInd+1],colList[colInd+1]+numList[numInd+1]]; //diagonals
		killerPos = checkIfPieceOnSq(validSqIds(killerPos0),sqBoard);
	};
	if ((fig.charAt(fig.length-2) == 'b')){
		if (num == 7 && checkIfPieceOnSq([col+numList[numInd-1],col+numList[numInd-2]],sqBoard).length == 0)
		{res = [col+numList[numInd-1],col+numList[numInd-2]]};

		if (checkIfPieceOnSq([col+numList[numInd-1]],sqBoard).length == 0)
		{res.push(col+numList[numInd-1]);};

		let killerPos0 = [colList[colInd-1]+numList[numInd-1],colList[colInd+1]+numList[numInd-1]]; //diagonals
		killerPos = checkIfPieceOnSq(validSqIds(killerPos0),sqBoard);
	};
	let finRes = validSqIds([].concat(res,killerPos));
	//console.log(finRes);
	return {'all':finRes,'attacableOnly':killerPos}
};




function horseSqueresAround(fig,figBoard) {
	let position = pos(fig,figBoard);
	let col = position[0];
	let num = position[1];
	let colInd = colList.indexOf(col);
	let numInd = numList.indexOf(num);
	let res =[
		colList[colInd-2]+numList[numInd+1],
		colList[colInd-2]+numList[numInd-1],
		colList[colInd+2]+numList[numInd+1],
		colList[colInd+2]+numList[numInd-1],
		colList[colInd-1]+numList[numInd+2],
		colList[colInd-1]+numList[numInd-2],
		colList[colInd+1]+numList[numInd+2],
		colList[colInd+1]+numList[numInd-2],
	];
	let finRes = validSqIds(res);
	return finRes
};

function diagOneSqueresAround(fig,figBoard) {
	let position = pos(fig,figBoard);
	let col = position[0];let num = position[1];
	let colInd = colList.indexOf(col);
	let numInd = numList.indexOf(num);
	let res =
	[colList[colInd-1]+numList[numInd-1],
	colList[colInd+1]+numList[numInd+1],
	colList[colInd-1]+numList[numInd+1],
	colList[colInd+1]+numList[numInd-1],];
	res = validSqIds(res);
	return res
};














