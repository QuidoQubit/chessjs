
function b_king_small_casteling(b_king_moved,sqBoard, figBoard) {
  let white_atackable_sqs = attacableSqsPerPlayer(figBoard, sqBoard)['w_all'];
  let final_pos = 'B8';
  if (
    pos('king_b1', figBoard) === 'D8' &&
    pos('rook_b1', figBoard) === 'A8' &&
    sqBoard['B8'] === 0 &&
    sqBoard['C8'] === 0 &&
    !white_atackable_sqs.includes('B8') &&
    !white_atackable_sqs.includes('C8') &&
    !white_atackable_sqs.includes('D8') &&
    b_king_moved === false
  ) {        //console.log(attacableSqsPerPlayer(gameLogBoard.slice(-1)[0],gameLogFigs.slice(-1)[0])));

    return final_pos;
  } else {
    return false;
  }
}

function w_king_small_casteling(w_king_moved,sqBoard, figBoard) {
  let black_atackable_sqs = attacableSqsPerPlayer(figBoard, sqBoard)['b_all'];
  let final_pos = 'B1';
  if (
    pos('king_w1', figBoard) === 'D1' &&
    pos('rook_w1', figBoard) === 'A1' &&
    sqBoard['B1'] === 0 &&
    sqBoard['C1'] === 0 &&
    !black_atackable_sqs.includes('B1') &&
    !black_atackable_sqs.includes('C1') &&
    !black_atackable_sqs.includes('D1') &&
    w_king_moved === false
  ) {
    return final_pos;
  } else {
    return false;
  }
}

function b_king_big_casteling(b_king_moved,sqBoard, figBoard) {
  let white_atackable_sqs = attacableSqsPerPlayer(figBoard, sqBoard)['w_all'];
  let final_pos = 'F8';
  if (
    pos('king_b1', figBoard) === 'D8' &&
    pos('rook_b2', figBoard) === 'H8' &&
    sqBoard['G8'] === 0 &&
    sqBoard['F8'] === 0 &&
    sqBoard['E8'] === 0 &&
    !white_atackable_sqs.includes('G8') &&
    !white_atackable_sqs.includes('F8') &&
    !white_atackable_sqs.includes('E8') &&
    !white_atackable_sqs.includes('D8') &&
    b_king_moved === false
  ) {
    return final_pos;
  } else {
    return false;
  }
}

function w_king_big_casteling(w_king_moved,sqBoard, figBoard) {
  let black_atackable_sqs = attacableSqsPerPlayer(figBoard, sqBoard)['b_all'];
  let final_pos = 'F1';
  if (
    pos('king_w1', figBoard) === 'D1' &&
    pos('rook_w2', figBoard) === 'H1' &&
    sqBoard['G1'] === 0 &&
    sqBoard['F1'] === 0 &&
    sqBoard['E1'] === 0 &&
    !black_atackable_sqs.includes('G1') &&
    !black_atackable_sqs.includes('F1') &&
    !black_atackable_sqs.includes('E1') &&
    !black_atackable_sqs.includes('D1') &&
    w_king_moved === false
  ) {
    return final_pos;
  } else {
    return false;
  }
}


function check(color,figBoard,sqBoard) { //zde chci take indikaci posice sachu
    //vraci zda je kral barvy color v sachu
	let sach;
	let anticolor;
	if (color == 'w'){anticolor = 'b';}else{anticolor='w'};
	//console.log(anticolor);
	let gameLog = attacableSqsPerPlayer(figBoard,sqBoard);
	let kingPos = pos('king_'+color+'1',figBoard);
	let attacable = gameLog[anticolor+'_all'];
	if (attacable.includes(kingPos)){sach = true;}else{sach = false;};
	if (sach == true){
        kingPosInCheck.push(kingPos);
        let startSq = document.getElementById(kingPos);
        if (startSq.className == "squere dark") {kingPosOrigoCol.push('HotPink')};
        if (startSq.className == "squere light") {kingPosOrigoCol.push('DarkOrchid')};};
	return sach
};

function backend_check(player,figBoard,sqBoard) { //backend sach bez zmeny barev policek
    //vraci zda je kral barvy color v sachu
	let sach; let color = player; let anticolor;
	if (color == 'w'){anticolor = 'b';}else{anticolor='w'};
	//console.log(anticolor);
	let gameLog = attacableSqsPerPlayer(figBoard,sqBoard);
	let kingPos = pos('king_'+color+'1',figBoard);
	let attacable = gameLog[anticolor+'_all'];
	if (attacable.includes(kingPos)){sach = true;}else{sach = false;};
	return sach
};

function allActiveFigIds(figBoard) {
	let figIds = [];
	for (f of figsAll){if (figBoard[f] != 0){figIds.push(f);};};
	return figIds
}

function uniqueArray(a) {
  return [...new Set(a)];
}

function attacableSqsPerPlayer(figBoard,sqBoard) { //gamelog
    //vraci atakovatelna pole
	var whiteAttacable = Object.assign({}, figDictEmptyW);
	var blackAttacable = Object.assign({}, figDictEmptyB);
	var whiteAll = [];
	var blackAll = [];
	for (const id of allActiveFigIds(figBoard) ){
		let fig = id;
		let type = figIdtoType(fig);
		if (type == 'Pawn'){var was = uniqueArray(attackableSqsPerFigType(fig,figBoard,sqBoard)['attacableOnly']);}else{
		var was = uniqueArray(attackableSqsPerFigType(fig,figBoard,sqBoard));};
		//console.log(was);
		if (fig.charAt(fig.length-2) == 'w'){whiteAttacable[fig] = whiteAttacable[fig].concat(was);whiteAll = whiteAll.concat(was);};
		if (fig.charAt(fig.length-2) == 'b'){blackAttacable[fig] = blackAttacable[fig].concat(was);blackAll = blackAll.concat(was);};
		};
	whiteAll = uniqueArray(whiteAll);
	blackAll = uniqueArray(blackAll);
	//console.log(whiteAll);
	return {'w_per_piece':whiteAttacable,'b_per_piece':blackAttacable,'w_all':whiteAll,'b_all':blackAll}
};

function possibleSqsPerPlayer(figBoard,sqBoard) { //gamelog
    //vraci vsechny mozne pohyby = stejne jako attacablePerPlayer ale pawn je zde "all"
	var whiteAttacable = Object.assign({}, figDictEmptyW);
	var blackAttacable = Object.assign({}, figDictEmptyB);
	var whiteAll = [];
	var blackAll = [];
	for (const id of allActiveFigIds(figBoard) ){
		let fig = id;
		let type = figIdtoType(fig);
		if (type == 'Pawn'){var was = uniqueArray(attackableSqsPerFigType(fig,figBoard,sqBoard)['all']);}else{
		var was = uniqueArray(attackableSqsPerFigType(fig,figBoard,sqBoard));};
		//console.log(was);
		if (fig.charAt(fig.length-2) == 'w'){whiteAttacable[fig] = whiteAttacable[fig].concat(was);whiteAll = whiteAll.concat(was);};
		if (fig.charAt(fig.length-2) == 'b'){blackAttacable[fig] = blackAttacable[fig].concat(was);blackAll = blackAll.concat(was);};
		};
	whiteAll = uniqueArray(whiteAll);
	blackAll = uniqueArray(blackAll);
	//console.log(whiteAll);
	return {'w_per_piece':whiteAttacable,'b_per_piece':blackAttacable,'w_all':whiteAll,'b_all':blackAll}
};

function pos(fig,figBoard) {return figBoard[fig]};

function figIdtoType(figId){
		let firstLett = figId.charAt(0).toUpperCase();
		let type = firstLett + figId.slice(1,figId.length-3);
		return type
};







