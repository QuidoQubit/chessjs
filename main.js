
const strict_mode = true; // if false, you can move figs out of allowed moves


var pieces = document.querySelectorAll('.chess-piece');
pieces.forEach(piece => {
	piece.addEventListener('click',startRound,true);
	piece.type = piece.alt;
	piece.color = piece.id.charAt(piece.id.length-2); // 'b' or 'w'
});

/* squere = na nem konci endRound */
const squeres = document.querySelectorAll('.squere');
squeres.forEach(squere => {
squere.addEventListener('click',endRound,false);
});

let beingSelected = []; //vybrana figurka
let toBeKilled = []; //vyhazuju
let origoCol = []; //barva ctverecku kde je selected
let origoSq = []; //kde je selected figurka
let markededSqsIds= []; // kam muze figurka
let markededSqsColors = []; //puvodni barvky kam muze figurka, aby po konci tahu mohli byt barvy zmeneny

var player = 'w';
var gameLogBoard = [sqBoard0]; //zde zapisuju celou herni pozici v kazdem tahu
var gameLogFigs = [figBoard0];

var czech = false;
var kingPosInCheck = [];
var kingPosOrigoCol = [];


var b_king_moved = false;
var w_king_moved = false;


//TODO = switcher povolenych tahu = mohu jen na zelene policka.
//TODO = pokud kral v sachu a cerveny, po kliknuti barva zmizi, chci aby zustala stejna dokud sach!
//TODO = definovat: sachmat,brani mimochodem
//TODO = prepis vse jako class :D


function startRound(e) { //e == event, clik na figurku

    if (gameLogFigs.slice(-1)[0]['king_w1'] !== 'D1'){w_king_moved = true}; //podminka rosady
    if (gameLogFigs.slice(-1)[0]['king_b1'] !== 'D8'){b_king_moved = true;}; // podminka rosady

	if (beingSelected.length == 0 && e.target.color == player){ //jeste jsem nevybral
		beingSelected.push(e.target);
		let startSq = document.getElementById(pos(beingSelected[0].id,gameLogFigs.slice(-1)[0]));
		let selectedIds;
		//pawn je jedina figurka,ktera ma rozdil mezi tim, kam muze vs kam muze utocit, rozdil k nalezeni v pawnSquaresAround
		if (figIdtoType(beingSelected[0].id) == 'Pawn'){
		selectedIds = attackableSqsPerFigType(beingSelected[0].id,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0])['all'];
		}
		else{
		selectedIds = attackableSqsPerFigType(beingSelected[0].id,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0]);};

		if (figIdtoType(beingSelected[0].id) == 'King') { //prida policko rosady pokud je mozno
            let small_casteling = false;
            let big_casteling = false;

				if (player == 'w') {small_casteling = w_king_small_casteling(w_king_moved,gameLogBoard.slice(-1)[0],gameLogFigs.slice(-1)[0]);
				 big_casteling = w_king_big_casteling(w_king_moved,gameLogBoard.slice(-1)[0],gameLogFigs.slice(-1)[0]);
				 }

                if (player == 'b') {small_casteling = b_king_small_casteling(b_king_moved,gameLogBoard.slice(-1)[0],gameLogFigs.slice(-1)[0]);
                 big_casteling = b_king_big_casteling(b_king_moved,gameLogBoard.slice(-1)[0],gameLogFigs.slice(-1)[0]);
                 }
            console.log(small_casteling,big_casteling);
    		if (small_casteling !== false) {selectedIds.push(small_casteling);};
    		if (big_casteling !== false) {selectedIds.push(big_casteling);}
		};

        //pryc origo posici
		selectedIds = selectedIds.filter(item => item !== startSq.id);
        selectedIds = uniqueArray(selectedIds);
         // pryc duplikaty
        selectedIds = remove_same_color_figs_from_selected(selectedIds,gameLogBoard.slice(-1)[0]);
        // pryc s pohyby, ktere by zpusobily sach
        markededSqsIds = realPossibleSqsToMove(beingSelected[0].id,selectedIds,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0]);

		markSqueres(markededSqsIds); // obarvy kam muze figurka

		//obarveni sq kde figurka stoji
		if (startSq.className == "squere dark") {origoCol.push('HotPink')};
		if (startSq.className == "squere light") {origoCol.push('DarkOrchid')};
		origoSq.push(startSq); //kde figurka stoji
		colorChangeSelectedFigSq(beingSelected[0],gameLogFigs.slice(-1)[0]);
		return //vratim se zpet, ale uz mam vybrano
	};
	//uz jsem vybral a mam dve moznosti:

	//pokud kliknu na policko s nepratelskou figurkou, nepratelskou pridam do toBeKilled selekce
	if ((beingSelected.length == 1) && (beingSelected[0].color != e.target.color)){toBeKilled.push(e.target)};
	//pokud znovuklinku na figurku stejne barvy, selekce se resetuje
  if ((beingSelected.length == 1) && (beingSelected[0].color == e.target.color)){resetVals();};
};

function endRound(e) { //event na Squere
    //je vybrano a nevyhazuju, jdu na prazdny Squere
	if ((e.target.className == "squere dark" || e.target.className == "squere light") &&
	    beingSelected.length == 1
        && strict_mode_control(e.target.id)
	    ){
		let figId = beingSelected[0].id;
		let newSqId = e.target.id;
        let changedBoard;
        // nejdriv zkus casteling, kdyz neni casteling, nic nemenim
        changedBoard = casteling_try(e,player,newSqId,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0]);

        //zmen gamelog na novou pozici
		changedBoard = boardChange(figId,newSqId,changedBoard['figBoard'],changedBoard['sqBoard']);
		//console.log(changedBoard['figBoard']['rook_w1']);
		gameLogFigs.push(changedBoard['figBoard']);
		gameLogBoard.push(changedBoard['sqBoard']);

		e.target.append(beingSelected[0]); //pridam figurku na novou pozici
		resetVals();
		changePlayer();
		//let check_control = check(player,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0]);
		czech = check(player,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0]);
		if (czech == true){ colorChangeKingInCheck(kingPosInCheck[0]);};
		console.log(czech);
	  }

    //je vybrano a vyhazuju
	if ((e.target.className == "chess-piece") &&
	    (toBeKilled.length == 1) &&
	    (beingSelected[0].color != toBeKilled[0].color)
        && strict_mode_control( pos(toBeKilled[0].id,gameLogFigs.slice(-1)[0]))
	    ){
		const killedPos = pos(toBeKilled[0].id,gameLogFigs.slice(-1)[0]);

		let killer = beingSelected[0];
		let killerId = killer.id;
		//odstran starou, presun figurku na novou pozici
		killFig(toBeKilled[0].id);
		addFig(killedPos,killer);

        //zmen gamelog na novou pozici
		let changedBoard = boardChange(killerId,killedPos,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0]);
		gameLogFigs.push(changedBoard['figBoard']);
		gameLogBoard.push(changedBoard['sqBoard']);

		resetVals();
		changePlayer();
		//let check_control = check(player,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0]);
		czech = check(player,gameLogFigs.slice(-1)[0],gameLogBoard.slice(-1)[0]);
		if (czech == true){ colorChangeKingInCheck(kingPosInCheck[0]);};
		console.log(czech);
	};
};

function strict_mode_control(sq_id){

    if (strict_mode == true){
        return markededSqsIds.includes(sq_id);
    }else{
        return true;
    };
};

// pomocne funkce:

function boardChange(movingFigId,movingToSqId,figBoard,sqBoard){
	let originalSq = figBoard[movingFigId]; // kde je figurka
	var killedFig; //figurka na pozici kam jdu
	let new_figBoard = Object.assign({}, figBoard); //copy
	let new_sqBoard = Object.assign({}, sqBoard); //copy

	if (new_sqBoard[movingToSqId] != 0){ //pokud je neco tam kam du
		killedFig = new_sqBoard[movingToSqId]; //... tak to neco je killedFig
		new_figBoard[killedFig] = 0; //killedFig uz neni
	};
	new_sqBoard[originalSq] = 0; // puvodni pozice je prazdna
	new_figBoard[movingFigId] = movingToSqId; //figurka ma novou pozici
	new_sqBoard[movingToSqId] = movingFigId; //pozice ma novou figurku
	return {'sqBoard':new_sqBoard,'figBoard':new_figBoard}
};

function changePlayer(){
	if (player == 'w'){player = 'b';
	let header = document.getElementById("info");
	header.style.color = 'Black';
	header.innerHTML = 'Black turn';
	return};
	if (player == 'b'){player = 'w';
	let header = document.getElementById("info");
	header.style.color = 'White';
	header.innerHTML = 'White turn';};
};

function addFig(squereId,fig) {const squere = document.getElementById(squereId);squere.append(fig);};

function killFig(figName) {const fuj = document.getElementById(figName);fuj.remove();};

function resetVals() {
	origoSq[0].style.backgroundColor = origoCol[0]; //vraci puvodni barvu kde stala selected figurka
	origoCol = [];
	beingSelected = []; //uz s nim nehybu
	toBeKilled = [];
	origoSq = [];
	unmarkSqueres(markededSqsIds); //vrati puvodni barvu atakovatelnych policek
	if (kingPosInCheck.length > 0){ //vrati barvu kde je sach a resetuje check vars
        document.getElementById(kingPosInCheck[0]).style.backgroundColor = kingPosOrigoCol[0];
        kingPosInCheck = [];
        kingPosOrigoCol = [];
    }
};


function remove_same_color_figs_from_selected(selectedIds,sqBoard) {
//toto muze byt upraveno tak, aby to jednou barvou oznacilo utocitelne a druhou chranene figurky treba...
//selectedIds = sq ids
    let res = [];
    for (sq of selectedIds){
        if (sqBoard[sq] == 0){
            res.push(sq);
        };
        if (sqBoard[sq] != 0){
        let fig = sqBoard[sq];
        let figcolor = fig.charAt(fig.length-2);
        if (figcolor != player){
                res.push(sq);
            }
        }
    }
    return res
}

function realPossibleSqsToMove(figId,selectedIds,figBoard,sqBoard) {
 //zkontroluj pro vsechny tahy figurky, ze nenastane sach
		let color = figId.charAt(figId.length-2);
        let possibleSqsToMove = [];
        for (s of selectedIds){
            let movingToSqId = s;
            new_board = boardChange(figId,movingToSqId,figBoard,sqBoard);
            new_figBoard = new_board['figBoard'];
            new_sqBoard = new_board['sqBoard'];
            if (backend_check(color,new_figBoard,new_sqBoard) == false){possibleSqsToMove.push(s);};
        };
        return possibleSqsToMove
}

function casteling_try(e,player,king_selected_SqId,figBoard,sqBoard) { //places the rook on the correct position
    //kral uz je vybran a presouva se na nove policko
    let origo_king_pos = figBoard['king_'+player+'1'];
    var board_changedBoard;
    let sq = king_selected_SqId;

    if (origo_king_pos == 'D1' && sq == 'B1'){
        board_changedBoard = boardChange('rook_w1','C1',figBoard,sqBoard);
        let rook_sq = document.getElementById('C1');
        rook_sq.append(document.getElementById('rook_w1'));
        return board_changedBoard
    }

    if (origo_king_pos == 'D1' && sq == 'F1'){
        board_changedBoard = boardChange('rook_w2','E1',figBoard,sqBoard);
        let rook_sq = document.getElementById('E1');
        rook_sq.append(document.getElementById('rook_w2'));
        return board_changedBoard
    }

    if (origo_king_pos == 'D8' && sq == 'B8'){
        board_changedBoard = boardChange('rook_b1','C8',figBoard,sqBoard);
        let rook_sq = document.getElementById('C8');
        rook_sq.append(document.getElementById('rook_b1'));
        return board_changedBoard
    }

    if (origo_king_pos == 'D8' && sq == 'F8'){
        board_changedBoard = boardChange('rook_b2','E8',figBoard,sqBoard);
        let rook_sq = document.getElementById('E8');
        rook_sq.append(document.getElementById('rook_b2'));
        return board_changedBoard
    }
    else {
        board_changedBoard = {'sqBoard':sqBoard,'figBoard':figBoard};
        return board_changedBoard
    };

}


