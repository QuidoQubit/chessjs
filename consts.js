const colList = ['A','B','C','D','E','F','G','H'];
const numList = ['1','2','3','4','5','6','7','8'];

const figsAll = [
'pawn_b1','pawn_b2','pawn_b3','pawn_b4','pawn_b5','pawn_b6','pawn_b7','pawn_b8','horse_b1','horse_b2','rook_b1','rook_b2','bishop_b1','bishop_b2','king_b1','queen_b1',
'pawn_w1','pawn_w2','pawn_w3','pawn_w4','pawn_w5','pawn_w6','pawn_w7','pawn_w8','horse_w1','horse_w2','rook_w1','rook_w2','bishop_w1','bishop_w2','king_w1','queen_w1',
];

var figBoard0 = {
	'pawn_b1':'A7',
	'pawn_b2':'B7',
	'pawn_b3':'C7',
	'pawn_b4':'D7',
	'pawn_b5':'E7',
	'pawn_b6':'F7',
	'pawn_b7':'G7',
	'pawn_b8':'H7',
	'horse_b1':'B8',
	'horse_b2':'G8',
	'rook_b1':'A8',
	'rook_b2':'H8',
	'bishop_b1':'C8',
	'bishop_b2':'F8',
	'king_b1':'D8',
	'queen_b1':'E8',

	'pawn_w1':'A2',
	'pawn_w2':'B2',
	'pawn_w3':'C2',
	'pawn_w4':'D2',
	'pawn_w5':'E2',
	'pawn_w6':'F2',
	'pawn_w7':'G2',
	'pawn_w8':'H2',
	'horse_w1':'B1',
	'horse_w2':'G1',
	'rook_w1':'A1',
	'rook_w2':'H1',
	'bishop_w1':'C1',
	'bishop_w2':'F1',
	'king_w1':'D1',
	'queen_w1':'E1',
};

var sqBoard0 = {
	'A8':'rook_b1','B8':'horse_b1','C8':'bishop_b1','D8':'king_b1','E8':'queen_b1','F8':'bishop_b2','G8':'horse_b2','H8':'rook_b2',
	'A7':'pawn_b1','B7':'pawn_b2','C7':'pawn_b3','D7':'pawn_b4','E7':'pawn_b5','F7':'pawn_b6','G7':'pawn_b7','H7':'pawn_b8',
	'A6':0,'B6':0,'C6':0,'D6':0,'E6':0,'F6':0,'G6':0,'H6':0,
	'A5':0,'B5':0,'C5':0,'D5':0,'E5':0,'F5':0,'G5':0,'H5':0,
	'A4':0,'B4':0,'C4':0,'D4':0,'E4':0,'F4':0,'G4':0,'H4':0,
	'A3':0,'B3':0,'C3':0,'D3':0,'E3':0,'F3':0,'G3':0,'H3':0,
	'A2':'pawn_w1','B2':'pawn_w2','C2':'pawn_w3','D2':'pawn_w4','E2':'pawn_w5','F2':'pawn_w6','G2':'pawn_w7','H2':'pawn_w8',
	'A1':'rook_w1','B1':'horse_w1','C1':'bishop_w1','D1':'king_w1','E1':'queen_w1','F1':'bishop_w2','G1':'horse_w2','H1':'rook_w2',
};

var figVals = {
	'pawn_b1':1,
	'pawn_b2':1,
	'pawn_b3':1,
	'pawn_b4':1,
	'pawn_b5':1,
	'pawn_b6':1,
	'pawn_b7':1,
	'pawn_b8':1,
	'horse_b1':2.5,
	'horse_b2':2.5,
	'rook_b1':5,
	'rook_b2':5,
	'bishop_b1':3,
	'bishop_b2':3,
	'king_b1':1000,
	'queen_b1':9,

	'pawn_w1':1,
	'pawn_w2':1,
	'pawn_w3':1,
	'pawn_w4':1,
	'pawn_w5':1,
	'pawn_w6':1,
	'pawn_w7':1,
	'pawn_w8':1,
	'horse_w1':2.5 ,
	'horse_w2':2.5,
	'rook_w1':5,
	'rook_w2':5,
	'bishop_w1':3,
	'bishop_w2':3,
	'king_w1':1000,
	'queen_w1':9,
};

const figDictEmptyB = {
	'pawn_b1':[],
	'pawn_b2':[],
	'pawn_b3':[],
	'pawn_b4':[],
	'pawn_b5':[],
	'pawn_b6':[],
	'pawn_b7':[],
	'pawn_b8':[],
	'horse_b1':[],
	'horse_b2':[],
	'rook_b1':[],
	'rook_b2':[],
	'bishop_b1':[],
	'bishop_b2':[],
	'king_b1':[],
	'queen_b1':[],
};

const figDictEmptyW = {
	'pawn_w1':[],
	'pawn_w2':[],
	'pawn_w3':[],
	'pawn_w4':[],
	'pawn_w5':[],
	'pawn_w6':[],
	'pawn_w7':[],
	'pawn_w8':[],
	'horse_w1':[],
	'horse_w2':[],
	'rook_w1':[],
	'rook_w2':[],
	'bishop_w1':[],
	'bishop_w2':[],
	'king_w1':[],
	'queen_w1':[],
};
